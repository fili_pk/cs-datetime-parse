use time::{macros::datetime, PrimitiveDateTime};

pub const TICK_MASK: u64 = 0x3FFFFFFFFFFFFFFF;
pub const MIN_DATE_TIME: PrimitiveDateTime = datetime!(1-1-1 0:00);
pub const MAX_DATE_TIME: PrimitiveDateTime = datetime!(9999-12-31 23:59:59.999999);
pub const TICKS_CEILING: u64 = 0x4000000000000000;

pub const MIN_TICKS: u64 = 0;
pub const MAX_TICKS: u64 = TICKS_PER_DAY * DAYS_TO_10000_YEAR as u64 - 1;
pub const DAYS_TO_10000_YEAR: i32 = 3652059;
pub const TICKS_PER_DAY: u64 = 24 * TICKS_PER_HOUR;
pub const TICKS_PER_HOUR: u64 = 60 * TICKS_PER_MINUTE as u64;
pub const TICKS_PER_MINUTE: u32 = 60 * TICKS_PER_SECOND;
pub const TICKS_PER_SECOND: u32 = 1000 * TICKS_PER_MILLISECOND as u32;
pub const TICKS_PER_MILLISECOND: u16 = 1000 * TICKS_PER_MICROSECOND as u16;
pub const TICKS_PER_MICROSECOND: u8 = (1000 / NANOSECONDS_PER_TICK as u16) as u8;
pub const NANOSECONDS_PER_TICK: u8 = 100;
